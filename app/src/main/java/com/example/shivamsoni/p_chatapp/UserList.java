package com.example.shivamsoni.p_chatapp;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.example.shivamsoni.p_chatapp.API.APIAsyncTask;
import com.example.shivamsoni.p_chatapp.API.UpdateComplete;
import com.example.shivamsoni.p_chatapp.Pojo.loginpojo;
import com.example.shivamsoni.p_chatapp.Pojo.userlistpojo;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

public class UserList extends AppCompatActivity implements UpdateComplete {

    private static final String TAG = "";
    public static String token;
    Context context;
    UpdateComplete updateComplete;
    ArrayList<userlistpojo> RegisteredUserList = new ArrayList<userlistpojo>();
    RecyclerView rvmain;
    String TOKEN;

    String urluser = "https://chat.promactinfo.com/api/user";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);

        context = this;
        updateComplete = (UpdateComplete) this;
        rvmain = (RecyclerView) findViewById(R.id.rvmain);

        token = getIntent().getStringExtra("loginToken");

        Toast.makeText(this, "" + token, Toast.LENGTH_SHORT).show();

        new APIAsyncTask(context, urluser, 0, null, true, null, "userlistpage", updateComplete, token, true, true).execute();


    }

    @Override
    public void onUpdateComplete(JSONObject resposne, String Ucode) {

        Toast.makeText(context, "obj res " + resposne, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onUpdateComplete(JSONArray resposne, String Ucode) {

        Log.d("logarray", String.valueOf(resposne));
//        Toast.makeText(context, "array res " + resposne, Toast.LENGTH_SHORT).show();

        JSONArray jarray = null;
        try {

            jarray = new JSONArray(String.valueOf(resposne));

            for (int i = 0; i < jarray.length(); i++) {

                JSONObject datas = jarray.getJSONObject(i);
                userlistpojo distribution = new userlistpojo();
                distribution.setName(datas.getString("name"));
                distribution.setId((Integer) datas.get("id"));

                RegisteredUserList.add(distribution);
            }
            empAdapter adapter = new empAdapter(RegisteredUserList, context);
            rvmain.setLayoutManager(new LinearLayoutManager(context));
            rvmain.setAdapter(adapter);



        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onUpdateFailed(String message, String Ucode) {

        Toast.makeText(context, "err " + message, Toast.LENGTH_SHORT).show();

    }
}
