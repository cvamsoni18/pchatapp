package com.example.shivamsoni.p_chatapp.API;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;


public class APIAsyncTask extends AsyncTask<Void, String, String> {

    private Context context;
    private ProgressDialog mProgressDialog;
    private String url, Ucode;
    UpdateComplete updateComplete;
    private boolean isProgressEnable, isToken;
    private int apiMethod = 1; // 0 for GET.  // 1 for POST.
    private boolean isBodyParam;
    private JSONObject paramData;
    private String token;
    private HashMap<String, String> hashparamData = new HashMap<String, String>();
//    private CustomLoader customLoader;

    public APIAsyncTask(Context context, String url, int apiMethod, JSONObject paramData, boolean isBodyParam, HashMap<String, String> HashParamData,
                        String Ucode, UpdateComplete updateComplete, String token, boolean isToken, boolean isProgress) {
        this.context = context;
        this.url = url;
        this.Ucode = Ucode;
        this.updateComplete = (UpdateComplete) updateComplete;
        isProgressEnable = isProgress;
        this.apiMethod = apiMethod;
        this.paramData = paramData;
        this.token = token;
        this.isToken = isToken;
        this.isBodyParam = isBodyParam;
        this.hashparamData = HashParamData;
    }

    @Override
    public void onPreExecute() {

    }

    @Override
    protected String doInBackground(Void... voids) {

        if (apiMethod == 1) {
            if (isToken ) {
                AndroidNetworking.post(url)
                        .addJSONObjectBody(paramData) // posting json
                        .setTag("DEMO")
                        .addQueryParameter(hashparamData)
                        .addHeaders("Authorization", token)
                        .setContentType("application/json")
                        .setPriority(Priority.HIGH)
                        .build()

                        .getAsJSONObject(new JSONObjectRequestListener() {
                            @Override
                            public void onResponse(JSONObject response) {

                                updateComplete.onUpdateComplete(response, Ucode);
                            }

                            @Override
                            public void onError(ANError anError) {

                                updateComplete.onUpdateFailed(anError.getErrorBody(), Ucode);

                            }

                        });


            } else {
                AndroidNetworking.post(url)
                        .addJSONObjectBody(paramData) // posting json
                        .setTag("DEMO")
                        .addHeaders("Authorization", token)
                        .setContentType("application/json")
                        .setPriority(Priority.HIGH)
                        .build()
                        .getAsJSONObject(new JSONObjectRequestListener() {
                            @Override
                            public void onResponse(JSONObject response) {
                                updateComplete.onUpdateComplete(response, Ucode);
                            }

                            @Override
                            public void onError(ANError anError) {
                                updateComplete.onUpdateFailed(anError.getErrorBody(), Ucode);

                            }
                        });
            }

        } else if (apiMethod == 0) {
            if (isToken  ) {
                AndroidNetworking.get(url)
                        .setTag("DEMO")
                        .addHeaders("Authorization", token)
                        .setPriority(Priority.HIGH)
                        .build()
                        .getAsJSONArray(new JSONArrayRequestListener() {
                            @Override
                            public void onResponse(JSONArray response) {
                                updateComplete.onUpdateComplete(response, Ucode);
                            }

                            @Override
                            public void onError(ANError anError) {
                                updateComplete.onUpdateFailed(anError.getErrorBody(), Ucode);

                            }
                        });


            } else {
                try {
                    AndroidNetworking.get(url)
                            .setTag("DEMO")
                            .addHeaders("Authorization", token)
                            .addQueryParameter(hashparamData)
                            .setPriority(Priority.HIGH)
                            .build()
                            .getAsJSONArray(new JSONArrayRequestListener() {
                                @Override
                                public void onResponse(JSONArray response) {
                                    updateComplete.onUpdateComplete(response, Ucode);
                                }

                                @Override
                                public void onError(ANError anError) {
                                    updateComplete.onUpdateFailed(anError.getErrorBody(), Ucode);

                                }
                            });

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }


        } else if (apiMethod == 1) {
            if (isToken ) {
                AndroidNetworking.post(url)
                        .addJSONObjectBody(paramData) // posting json
                        .setTag("DEMO")
                        .setContentType("application/json")
                        .addHeaders("Authorization", token)
                        .setPriority(Priority.HIGH)
                        .build()
                        .getAsJSONArray(new JSONArrayRequestListener() {
                            @Override
                            public void onResponse(JSONArray response) {
                                updateComplete.onUpdateComplete(response, Ucode);
                            }

                            @Override
                            public void onError(ANError anError) {
                                updateComplete.onUpdateFailed(anError.getErrorBody(), Ucode);

                            }
                        });
            } else {
                AndroidNetworking.post(url)
                        .addJSONObjectBody(paramData) // posting json
                        .setTag("DEMO")
                        .addHeaders("Authorization", token)
                        .setPriority(Priority.HIGH)
                        .build()
                        .getAsJSONArray(new JSONArrayRequestListener() {
                            @Override
                            public void onResponse(JSONArray response) {
                                updateComplete.onUpdateComplete(response, Ucode);
                            }

                            @Override
                            public void onError(ANError anError) {
                                updateComplete.onUpdateFailed(anError.getErrorBody(), Ucode);

                            }
                        });
            }


        }
        else if (apiMethod == 0) {
            if (isToken  ) {
                AndroidNetworking.get(url)
                        .setTag("DEMO")
                        .addHeaders("Authorization", token)
                        .setPriority(Priority.HIGH)
                        .build()

                        .getAsJSONObject(new JSONObjectRequestListener() {
                            @Override
                            public void onResponse(JSONObject response) {
                                updateComplete.onUpdateComplete(response, Ucode);
                            }

                            @Override
                            public void onError(ANError anError) {
                                updateComplete.onUpdateFailed(anError.getErrorBody(), Ucode);
                            }
                        });

            } else {
                try {
                    AndroidNetworking.get(url)
                            .setTag("DEMO")
                            .addHeaders("Authorization", token)
                            .addQueryParameter(hashparamData)
                            .setPriority(Priority.HIGH)
                            .build()
                            .getAsJSONArray(new JSONArrayRequestListener() {
                                @Override
                                public void onResponse(JSONArray response) {
                                    updateComplete.onUpdateComplete(response, Ucode);
                                }

                                @Override
                                public void onError(ANError anError) {
                                    updateComplete.onUpdateFailed(anError.getErrorBody(), Ucode);

                                }
                            });

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }


        }
        return null;
    }
}