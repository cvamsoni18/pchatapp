package com.example.shivamsoni.p_chatapp.API;

import org.json.JSONArray;
import org.json.JSONObject;


public interface UpdateComplete {
    void onUpdateComplete(JSONObject resposne, String Ucode);

    void onUpdateComplete(JSONArray resposne, String Ucode);

    void onUpdateFailed(String message, String Ucode);
}