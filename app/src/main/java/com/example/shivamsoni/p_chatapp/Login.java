package com.example.shivamsoni.p_chatapp;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.shivamsoni.p_chatapp.API.APIAsyncTask;
import com.example.shivamsoni.p_chatapp.API.UpdateComplete;
import com.example.shivamsoni.p_chatapp.Pojo.loginpojo;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Login extends AppCompatActivity implements UpdateComplete {

    private static final String TAG =" " ;
    Context context;
    UpdateComplete updateComplete;
    EditText edtusername;
    Button btnlogin;
    String TOKEN;

    String url="https://chat.promactinfo.com/api/user/login";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        context = this;
        updateComplete = (UpdateComplete) this;

        edtusername=(EditText)findViewById(R.id.edtusername);
        btnlogin=(Button)findViewById(R.id.btnlogin);

        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                JSONObject mDataObject = null;
                mDataObject = new JSONObject();
                try {
                    mDataObject.put("Name",edtusername.getText());

                    Log.e("Login Request", String.valueOf(mDataObject));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                new APIAsyncTask(context,url,1,mDataObject,true,null,"loginpage",updateComplete,null,false,true).execute();

            }
        });


    }

    @Override
    public void onUpdateComplete(JSONObject resposne, String Ucode) {

//        Toast.makeText(context, "res obj "+resposne, Toast.LENGTH_SHORT).show();

        Gson gson = new Gson();
        loginpojo gsonparse = gson.fromJson(String.valueOf(resposne), loginpojo.class);

//        Toast.makeText(context, "Token "+gsonparse.getToken(), Toast.LENGTH_SHORT).show();
        TOKEN = gsonparse.getToken();

        if (TOKEN !=null) {

            Intent i = new Intent(context, UserList.class);
            i.putExtra("loginToken", TOKEN);
            context.startActivity(i);
        }
    }

    @Override
    public void onUpdateComplete(JSONArray resposne, String Ucode) {

        Toast.makeText(context, "res array"+resposne, Toast.LENGTH_SHORT).show();
        Log.d(TAG, "loginresponce "+resposne);


    }

    @Override
    public void onUpdateFailed(String message, String Ucode) {

        Toast.makeText(context, "err "+message, Toast.LENGTH_SHORT).show();
        Log.d(TAG, "Login error responce: "+message);


    }
}
