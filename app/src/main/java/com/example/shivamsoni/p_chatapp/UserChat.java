package com.example.shivamsoni.p_chatapp;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.example.shivamsoni.p_chatapp.API.APIAsyncTask;
import com.example.shivamsoni.p_chatapp.API.UpdateComplete;
import com.example.shivamsoni.p_chatapp.Pojo.ToChatPojo;
import com.example.shivamsoni.p_chatapp.Pojo.userlistpojo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.example.shivamsoni.p_chatapp.UserList.token;

public class UserChat extends AppCompatActivity implements UpdateComplete {

    private static final String TAG = "";
    Context context;
    UpdateComplete updateComplete;
    ArrayList<ToChatPojo> messagesToList = new ArrayList<ToChatPojo>();
    RecyclerView rvmainchat;

    public static String ToId;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_chat);

        context = this;
        updateComplete = (UpdateComplete) this;
        rvmainchat = (RecyclerView) findViewById(R.id.rvmainchat);

        ToId = getIntent().getStringExtra("touserid");

//        Toast.makeText(context, "To id: "+ToId, Toast.LENGTH_SHORT).show();

//        Toast.makeText(this, "Token: " + token, Toast.LENGTH_SHORT).show();

        String urluserChat = "https://chat.promactinfo.com/api/chat/"+ToId+"";

        new APIAsyncTask(context, urluserChat, 0, null, true, null, "userchat", updateComplete, token, true, true).execute();
    }

    @Override
    public void onUpdateComplete(JSONObject resposne, String Ucode) {

        Toast.makeText(context, "obj res " + resposne, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onUpdateComplete(JSONArray resposne, String Ucode) {

        Toast.makeText(context, "array res chat " + resposne, Toast.LENGTH_SHORT).show();
        Log.d("array res chat", String.valueOf(resposne));

        JSONArray jarray = null;
        try {

            jarray = new JSONArray(String.valueOf(resposne));


            for (int i = 0; i < jarray.length(); i++) {

                JSONObject datas = jarray.getJSONObject(i);
                ToChatPojo messagesTo = new ToChatPojo();
                messagesTo.setMessage(datas.getString("message"));
                messagesTo.setCreatedDateTime(datas.getString("createdDateTime"));

                messagesToList.add(messagesTo);
            }
            ToChatAdapter adapter = new ToChatAdapter(messagesToList, context);
            rvmainchat.setLayoutManager(new LinearLayoutManager(context));
            rvmainchat.setAdapter(adapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onUpdateFailed(String message, String Ucode) {

    }
}
