package com.example.shivamsoni.p_chatapp;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shivamsoni.p_chatapp.Pojo.ToChatPojo;
import com.example.shivamsoni.p_chatapp.Pojo.userlistpojo;

import java.util.List;

public class ToChatAdapter extends RecyclerView.Adapter<ToChatAdapter.MyViewHolder> {

//    private ArrayList<userlistpojo> moviesList;
    private List<ToChatPojo> moviesList;
    String ToUserId;

    Context ctx;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView message,date;

        public MyViewHolder(View view) {
            super(view);
            message = (TextView) view.findViewById(R.id.messageto);
            date = (TextView) view.findViewById(R.id.date);
        }
    }


    public ToChatAdapter(List<ToChatPojo> moviesList, Context ctx) {
        this.moviesList = moviesList;
        this.ctx = ctx;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.userchat_raw, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final ToChatPojo movie = moviesList.get(position);

        holder.message.setText(movie.getMessage());
        holder.date.setText(movie.getCreatedDateTime());

//        holder.username.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(ctx, ""+movie.getId(), Toast.LENGTH_SHORT).show();
//
//                ToUserId= String.valueOf(movie.getId());
//
//                Intent i = new Intent(ctx, UserChat.class);
//                i.putExtra("touserid", ToUserId);
//                ctx.startActivity(i);
//            }
//        });



    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}