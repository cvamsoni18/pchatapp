package com.example.shivamsoni.p_chatapp;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shivamsoni.p_chatapp.Pojo.userlistpojo;

import java.util.ArrayList;
import java.util.List;

import static android.support.constraint.Constraints.TAG;

public class empAdapter extends RecyclerView.Adapter<empAdapter.MyViewHolder> {

//    private ArrayList<userlistpojo> moviesList;
    private List<userlistpojo> moviesList;
    String ToUserId;

    Context ctx;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView username;

        public MyViewHolder(View view) {
            super(view);
            username = (TextView) view.findViewById(R.id.usernametv);
        }
    }


    public empAdapter( List<userlistpojo> moviesList,Context ctx) {
        this.moviesList = moviesList;
        this.ctx = ctx;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_raw, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final userlistpojo movie = moviesList.get(position);

        holder.username.setText(movie.getName());

        holder.username.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ctx, ""+movie.getId(), Toast.LENGTH_SHORT).show();

                ToUserId= String.valueOf(movie.getId());

                Intent i = new Intent(ctx, UserChat.class);
                i.putExtra("touserid", ToUserId);
                ctx.startActivity(i);
            }
        });



    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}